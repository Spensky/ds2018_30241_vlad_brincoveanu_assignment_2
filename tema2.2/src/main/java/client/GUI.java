package client;

import rmi.Car;
import rmi.ISellingService;
import rmi.ITaxService;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class GUI {
    public GUI(final ITaxService taxService, final ISellingService sellingService) {
        final JFrame guiFrame = new JFrame();
//make sure the program exits when the frame closes
        guiFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        guiFrame.setTitle("Car price calculator");
        guiFrame.setSize(150, 500);
//This will center the JFrame in the middle of the screen
        guiFrame.setLocationRelativeTo(null);
        JPanel panelYear = new JPanel();
        JLabel labelYear = new JLabel("Year:");
        final JTextField yearText = new JTextField();
        yearText.setPreferredSize(new Dimension(120, 40));
        panelYear.add(labelYear);
        panelYear.add(yearText);

        JPanel panelEngine = new JPanel();
        JLabel labelEngine = new JLabel("EngineSize:");
        final JTextField engineText = new JTextField();
        engineText.setPreferredSize(new Dimension(120, 40));
        panelYear.add(labelEngine);
        panelYear.add(engineText);

        JPanel panelSize = new JPanel();
        JLabel labelPrice = new JLabel("Price:");
        final JTextField priceText = new JTextField();
        priceText.setPreferredSize(new Dimension(120, 40));
        panelYear.add(labelPrice);
        panelYear.add(priceText);

        JButton buttonSelling = new JButton("Calculate SP");
        JButton buttonTax = new JButton("Calculate Tax");
        buttonSelling.setPreferredSize(new Dimension(120, 120));
        buttonTax.setPreferredSize(new Dimension(120, 120));

        panelYear.add(buttonSelling);
        panelYear.add(buttonTax);

        guiFrame.add(panelYear);
//        guiFrame.add(panelEngine);
//        guiFrame.add(panelSize);
        guiFrame.setVisible(true);

        buttonTax.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String textYear = yearText.getText();
                        String textEngine = engineText.getText();
                        String textPrice = priceText.getText();
                        double tax = 0;
                        try {
                            tax = taxService.computeTax(
                                    new Car(Integer.parseInt(textYear),
                                            Integer.parseInt(textEngine),
                                            Integer.parseInt(textPrice)));
                        } catch (RemoteException e1) {
                            e1.printStackTrace();
                        }
                        JOptionPane.showMessageDialog(guiFrame,"Compute Tax "+ tax);
            }
        });

        buttonSelling.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textYear = yearText.getText();
                String textEngine = engineText.getText();
                String textPrice = priceText.getText();

                double sellingPrice = 0;
                try {
                    sellingPrice = sellingService.sellingPrice(
                            new Car(Integer.parseInt(textYear),
                                    Integer.parseInt(textEngine),
                                    Integer.parseInt(textPrice)));
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
                sellingPrice = Math.round(sellingPrice * 100.0) / 100.0;
                JOptionPane.showMessageDialog(guiFrame,"Selling Price "+ sellingPrice);
            }
        });
    }

}
