package client;

import rmi.Car;
import rmi.ISellingService;
import rmi.ITaxService;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientStart {
    public static void main(String[] args) {
        ITaxService taxService;
        ISellingService sellingService;
        try {
            Registry registry = LocateRegistry.getRegistry();
            sellingService = (ISellingService) registry
                    .lookup("SellingService");
            taxService = (ITaxService) registry
                    .lookup("TaxService");
            System.out.println(taxService.computeTax(new Car(2008,1600,3200)));
            System.out.println(sellingService.sellingPrice(new Car(2012,1600,3200)));
            new GUI(taxService,sellingService);
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
        }
    }
}
