package client;

import javax.swing.*;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class Guie {
    private JTextField textYear;
    private JLabel Year;
    private JTextField textEngineSize;
    private JLabel EngineSize;
    private JTextField textPrice;
    private JLabel Price;
    private JButton Tax;
    private JButton Selling;

    public Guie(){
        final JFrame guiFrame = new JFrame();
//make sure the program exits when the frame closes
        guiFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        guiFrame.setTitle("Car price calculator");
        guiFrame.setSize(300, 300);
//This will center the JFrame in the middle of the screen
        guiFrame.setLocationRelativeTo(null);

        JPanel jPanel = new JPanel();
        jPanel.add(textYear);
        jPanel.add(Year);
        jPanel.add(textEngineSize);
        jPanel.add(EngineSize);
        jPanel.add(textPrice);
        jPanel.add(Price);
        jPanel.add(Tax);
        jPanel.add(Selling);


        guiFrame.setVisible(true);
    }
}
