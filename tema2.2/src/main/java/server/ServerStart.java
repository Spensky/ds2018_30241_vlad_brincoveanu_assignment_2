package server;

import rmi.Car;
import rmi.ISellingService;
import rmi.ITaxService;
import server.services.SellingService;
import server.services.TaxService;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerStart {
    public static void main(String[] args) {
        ISellingService stub;
        ITaxService stub1;

        try {
            SellingService obj = new SellingService();
            TaxService obj1 = new TaxService();
            stub = (ISellingService) UnicastRemoteObject.exportObject(obj, 0);
            stub1 = (ITaxService) UnicastRemoteObject.exportObject(obj1, 0);

            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("SellingService", stub);
            registry.rebind("TaxService", stub1);

            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server error: " + e.getMessage());
        }
    }
}
