package server.services;

import rmi.Car;
import rmi.ISellingService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class SellingService implements ISellingService{
    private static final long serialVersionUID = 1L;
    public SellingService() throws RemoteException {
    }

    @Override
    public double sellingPrice(Car c) {
        double calculus = c.getPrice() - c.getPrice()/7 * (2018-c.getYear());
        return 2018-c.getYear() < 7 ? calculus : 0;
    }
}
