package server.services;

import rmi.Car;
import rmi.ITaxService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class TaxService implements ITaxService {
    private static final long serialVersionUID = 1L;

    public TaxService() throws RemoteException {
    }

    @Override
    public double computeTax(Car c) {
        double tax = 0;

       if (c.getEngineSize() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if (c.getEngineSize() > 1601) sum = 18;
        if (c.getEngineSize() > 2001) sum = 72;
        if (c.getEngineSize() > 2601) sum = 144;
        if (c.getEngineSize() > 3001) sum = 290;

        tax = (c.getEngineSize() / 200.0) * sum;
        return tax;
    }
}
