package rmi;

import java.io.Serializable;
import java.util.Objects;

public class Car implements Serializable {
    private static final long serialVersionUID = 1L;
    private int year;
    private int engineSize;
    private double price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return year == car.year &&
                engineSize == car.engineSize &&
                Double.compare(car.price, price) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, engineSize, price);
    }

    @Override
    public String toString() {
        return "Car{" +
                "year=" + year +
                ", engineSize=" + engineSize +
                ", price=" + price +
                '}';
    }

    public Car(int year, int engineSize, double price) {
        this.year = year;
        this.engineSize = engineSize;
        this.price = price;
    }

    public Car(){

    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(int engineSize) {
        this.engineSize = engineSize;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
