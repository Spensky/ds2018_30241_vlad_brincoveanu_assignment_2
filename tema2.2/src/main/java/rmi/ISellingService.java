package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISellingService extends Remote {
    double sellingPrice(Car c) throws RemoteException;
}
