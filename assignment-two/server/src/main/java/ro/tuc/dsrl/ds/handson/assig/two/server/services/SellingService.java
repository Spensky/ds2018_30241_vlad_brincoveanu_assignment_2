package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISellingPrice;

public class SellingService implements ISellingPrice {
    @Override
    public double sellingPrice(Car c) {
        double calculus = c.getPurchacesPrice() - c.getPurchacesPrice()/7 * (2018-c.getYear());
        return 2018-c.getYear() < 7 ? calculus : 0;
    }
}
